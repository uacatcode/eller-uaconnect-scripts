﻿<#
.SYNOPSIS
  This script searches an OU and looks for @emailarizona.mail.onmicrosoft.com and creates the alias if it doesn't exist.
.DESCRIPTION
  This script searches an OU and looks for @emailarizona.mail.onmicrosoft.com and creates the alias if it doesn't exist.
  Powershell script needs to be executed through exchange management shell. ResultSetSize is set to 1000 and can be increased if needed.
#>

#Script Name Add_O365Alias.ps1
#Creator Shawn Kolofer


#Declare Variables
$SearchBase = “OU=Delegated OUs,OU=Delegation,DC=catnet,DC=arizona,DC=edu” #setThis
$Accounts = Get-ADUser -SearchBase $SearchBase -Filter * -ResultSetSize 1000

#Loop Through OU's Accounts
foreach($a in $Accounts)
{
    #Grab UPN and SamAccountName
    $UPN = $a.UserPrincipalName
    $SamAccountName = $a.SamAccountName

    #Configure @onmicrosoft.com Address
    $SMTPAddress = "smtp:" + $SamAccountName + "@emailarizona.mail.onmicrosoft.com"

    #Pull existing SMTP Addresses
    $Emails = Get-Recipient $UPN | Select Name -ExpandProperty EmailAddresses
    $Exists = 0

    #Loop Through SMTP Addresses and see if @emailarizona.mail.onmicrosoft.com exists
    foreach($e in $Emails)
    {
        if($e -eq $SMTPAddress)
        {
            $Exists = 1
        }
    }

    #Add alias if missing
    if($Exists -eq 0)
    {
        Set-Mailbox $UPN -EmailAddresses @{add=$SMTPAddress}
        Write-Host "Added $SMTPAddress"
    }
}
