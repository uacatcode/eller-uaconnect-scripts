﻿<#
.SYNOPSIS
  Script searches an OU to audits shared mailboxes. It pulls delegated admins, inbox permission, and emails all admins. Script must be run from Exchange Management Shell.
.DESCRIPTION
  Script searches an OU to audits shared mailboxes. It pulls delegated admins, inbox permission, and emails all admins. Script must be run from Exchange Management Shell.
  Add your email address to the Send-MailMessage for testing.
#>

#Script Name SharedBox_Audit.ps1
#Creator Shawn Kolofer


#Declare Variables
$SearchBase = “OU=Delegated OUs,OU=Delegation,DC=catnet,DC=arizona,DC=edu”
$SharedBoxes = Get-ADUser -SearchBase $SearchBase -Filter *

#Email Configuration
$FromEmailAddress = "" #setThis
$BccEmailAddress = "" #setThis
$ToTestEmailAddress = "" #setThis

$EmailSubject = "[Eller IT] Shared Calendar and Mailbox Verification"#setThis
$Intro = "Eller IT is in the process of auditing all of Eller's shared calendars, and mailboxes inside of the UAConnect environment in preparation for the UA Office 365 email migration. If this mailbox is no longer needed, please reply to this email saying so, and we will remove it.<br><br>
Below is a list of everyone who currently has access to the calendar. If you find anyone on this list that should no longer have access <b>please remove them by May 31st</b>.<br><br>" #setThis
$Closing = "<br>Thank you for taking the time to help us keep Eller's calendars and mailboxes current, and please let us know if you have any questions or concerns. <br><br>" #setThis
$Signature = "--<br>Eller Information Technology Department<br>The University of Arizona<br><br>To request support for a new issue, please visit http://support.eller.arizona.edu</FONT></body>" #setThis


#Loop through Calendar OU
foreach($s in $SharedBoxes)
{

#format email
$emailText = @"
<style>table{border-width: 1px;border-style: solid;border-color: black;border-collapse: collapse;}th{border-width: 2px;padding: 0px;border-style: solid;border-color: black;}td{border-width: 1px;padding: 5px;border-style: solid;border-color: black;}</style>
"@


    #Create Table in Powershell
    $table = New-Object System.Data.DataTable "PermissionsList"
    $col1 = New-Object system.Data.DataColumn UserList,([string])
    $col2 = New-Object system.Data.DataColumn AccessRights,([string])
    $table.columns.add($col1)
    $table.columns.add($col2)

    #Declare additional variables
    $MailBox = $s.Name
    $SamAccountName = $s.SamAccountName
    $Owners = New-Object -TypeName System.Collections.Generic.List[string]
    $Owners2 = New-Object -TypeName System.Collections.Generic.List[string]
    $FirstNames = New-Object -TypeName System.Collections.Generic.List[string]
    $UserList = New-Object -TypeName System.Collections.Generic.List[string]
    $PermissionList = New-Object -TypeName System.Collections.Generic.List[string]
    $TotalUsers = 0
    $TotalOwners = 0
    $TotalFirst = 0
    $Inbox = $SamAccountName + ":\Inbox"

    #Delegates are listed as Owners, CalPermissions are Users and Permissions to each calendar
    $Delegates = Get-Mailbox -identity $MailBox | Get-MailboxPermission | where {$_.user.tostring() -ne "NT AUTHORITY\SELF" -and $_.IsInherited -eq $false} | Select User,AccessRights
    $Permissions = Get-MailboxFolderPermission -identity $Inbox | where {$_.user.tostring() -ne "Default"} | where {$_.user.tostring() -ne "Anonymous"} | Select User,AccessRights


    #Parsing for UPN and Display Name
    foreach($d in $Delegates)
    {
        if($d.AccessRights -eq "FullAccess")
        {
            $User = $d.User.Replace("CATNET\","")
            $UPN = Get-ADUser -Identity $User
            $Owners.Add($UPN.UserPrincipalName)
            $Owners2.Add($UPN.Name)
            $FirstNames.Add($UPN.GivenName)
        }
    }

    #Parsing for Username, Permissions, and adding to List
    foreach($c in $Permissions)
    {
        $LoopCount = 0
        $Access = ""
        $UserName = $c.User
        $UserName = $UserName.DisplayName
        $recipients = ""
        foreach($a in $c.AccessRights)
        {
            if($LoopCount -eq 0)
            {
                $Access = $a
            }
            if($LoopCount -ne 0)
            {
                $Access = $Access + ";" + $a
            }
            $LoopCount = $LoopCount + 1
        }
        $UserList.Add($UserName)
        $PermissionList.Add($Access)
        $TotalUsers = $TotalUsers + 1
    }

    #Parsing for the Email Recipients List
    foreach($o in $Owners)
    {
        if($TotalOwners -eq 0)
        {
            $recipients = $o
        }

        if($TotalOwners -gt 0)
        {
            $recipients = $recipients + ", " + $o
        }
        $TotalOwners = $TotalOwners + 1
    }

    $LastUser = $TotalOwners - 1
    $TwoUsers = 0
    #Parsing for the FirstNames
    foreach($f in $FirstNames)
    {
        if($TotalFirst -eq 0)
        {
            $FirstName = $f
        }
        if($TotalFirst -eq 1 -and $TotalFirst -eq $LastUser)
        {
            $FirstName = $FirstName + " and " + $f
            $TwoUsers = 1
        }
        if($TotalFirst -eq $LastUser -and $TotalFirst -gt 0 -and $TwoUsers -eq 0)
        {
            $FirstName = $FirstName + ", " + "and " + $f
        }
        if($TotalFirst -gt 0 -and $TotalFirst -ne $LastUser)
        {
            $FirstName = $FirstName + ", " + $f
        }
        $TotalFirst = $TotalFirst + 1
    }

    #Create Table for Delegated Admins
    for($OwnerCount = 0; $OwnerCount -lt $TotalOwners; $OwnerCount++)
    {
        $row1 = $table.NewRow()
        $row1.UserList = $Owners2[$OwnerCount]
        $row1.AccessRights = "Owner"
        $table.Rows.Add($row1)
    }

    #Create Table User Permissions
    for($UserCount = 0; $UserCount -lt $TotalUsers; $UserCount++)
    {
        $Added = 0
        foreach($o in $Owners2)
        {
            if($o -eq $UserList[$UserCount])
            {
            $Added = 1
            }
        }
        if($Added -eq 0)
        {
            $row = $table.NewRow()
            $row.UserList = $UserList[$UserCount]
            $row.AccessRights = $PermissionList[$UserCount]
            $table.Rows.Add($row)
        }
    }

    #Email Intro
    $emailText += "<FONT FACE ='calibri'>Hi " + $FirstName + ",<br><br>You are receiving this email because you have the ability to manage permissions to the &quot;$MailBox&quot; mailbox. "
    $emailText += $Intro

    #Format the HTML Table
    $emailText += "<table><tr><td>Name</td><td>Access Level</td></tr>"

    #Add table to Email
    foreach ($row in $table.Rows)
    {
        $emailText += "<tr><td>" + $row[0] + "</td><td>" + $row[1] + "</td></tr>"
    }
    $emailText += "</table><br>"

    #Add Closing and Signature
    $EmailSubject2 = $EmailSubject + " - $Mailbox"
    $emailText += $Closing
    $emailText += $Signature

    #Send Email
    #send-mailmessage -to $Owners -from $FromEmailAddress -Bcc $BccEmailAddress -subject $EmailSubject -Body $emailText -BodyAsHTML -smtpserver smtpgate.email.arizona.edu #setThis
    Write-Host "$Mailbox Email Sent to $recipients"

    #Send Test Emails
    send-mailmessage -to $ToTestEmailAddress -from $FromEmailAddress -subject $EmailSubject2 -Body $emailText -BodyAsHTML -smtpserver smtpgate.email.arizona.edu #setThis

    #Break - Remove/Comment below to loop
    Read-Host
}
