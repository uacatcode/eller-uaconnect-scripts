<#
.SYNOPSIS
  Script searches a scope group, and it outputs any added permissions to the inbox or default calendar.
.DESCRIPTION
  Script searches a scope group. It pulls permissions granted to the inbox and default calendar. Script must be run from Exchange Management Shell.  Output is saved to a semi-colon separated file.
#>

#Script Name listUserMailboxPermissions.ps1
#Creator Paul Reeves (with help from Shawn Kolofer)

#Declare Variables
$unitScopeGroup = "" #This can be set to any security group, such as the NetID scope groups in Catnet > Enterprise > Groups > Exchange > NetIDScopeGroups
$scopeGroupMailboxes = Get-ADGroupmember $unitScopeGroup
$NetIDRegEx = [regex]"((?<=\()[a-z]{1,20}(?=\)))"
$outputFileName = 'sharedObjectPermissions.txt'

#start output file
'Name; NetID; Mailbox/Calendar Permission; Access Rights; Permission Holder NetID; Permission Holder Name; ' | Out-file $outputFileName

#Loop through users
foreach($user in $scopeGroupMailboxes) {

  #Test to make sure whether a mailbox exists for that user.
  $mailboxExists = [bool](get-mailbox -recipienttype usermailbox $user.name -erroraction SilentlyContinue)
  if (-not $mailboxExists){continue}

  #Get all inbox permissions
  $userInbox = $user.SamAccountName + ":\Inbox"
  $inboxPermissions = Get-MailboxFolderPermission -identity $userInbox | where {$_.user.tostring() -ne "Default"} | where {$_.user.tostring() -ne "Anonymous"} | Select User,AccessRights

  #Output inbox permissions
  foreach ($inboxPermission in $inboxPermissions){
    $userName = $user.name.substring(0,$user.name.length - ($user.samaccountname.length + 4))

    $match = $NetIDRegEx.match($inboxPermission.User.DisplayName)
    $permissionHolderNetID = $match.Value

    $permissionHolderFullName = $inboxPermission.User.DisplayName.substring(0, $inboxPermission.User.DisplayName.length - ($permissionHolderNetID.length + 4))

    if ($permissionHolderNetID.length -eq 0){$permissionHolderFullName = $inboxPermission.User.DisplayName }

    $user.samaccountname + "; " + $userName + "; Mailbox; " + $inboxPermission.AccessRights + "; " + $permissionHolderNetID + "; " + $permissionHolderFullName | out-file -append $outputFileName
  }

  #Get all calendar permissions
  $userCalendar = $user.SamAccountName + ":\Calendar"
  $calendarPermissions = Get-MailboxFolderPermission -identity $userCalendar | where {$_.user.tostring() -ne "Default"} | where {$_.user.tostring() -ne "Anonymous"} | Select User,AccessRights

  #Output calendar permissions
  foreach ($calendarPermission in $calendarPermissions){
    $userName = $user.name.substring(0,$user.name.length - ($user.samaccountname.length + 4))

    $match = $NetIDRegEx.match($calendarPermission.User.DisplayName)
    $permissionHolderNetID = $match.Value

    $permissionHolderFullName = $calendarPermission.User.DisplayName.substring(0, $calendarPermission.User.DisplayName.length - ($permissionHolderNetID.length + 4))

    if ($permissionHolderNetID.length -eq 0){$permissionHolderFullName = $inboxPermission.User.DisplayName }

    $user.samaccountname + "; " + $userName + "; Calendar; " + $calendarPermission.AccessRights + "; " + $permissionHolderNetID + "; " + $permissionHolderFullName | out-file -append $outputFileName
  }

}
