﻿<#
.SYNOPSIS
  This script searches an OU and looks for uaid attribute needed for O365 to sync. If the attribute does not exist the script will add the GUID to uaid property.
.DESCRIPTION
  This script searches an OU and looks for uaid attribute needed for O365 to sync. If the attribute does not exist the script will add the GUID to uaid property.
  ResultSetSize is set to 1000 and can be increased if needed.
#>

#Script Name Add_O365uaid.ps1
#Creator Shawn Kolofer


#Declare Variables
$SearchBase = “OU=Delegated OUs,OU=Delegation,DC=catnet,DC=arizona,DC=edu” #setThis
$Accounts = Get-ADUser -SearchBase $SearchBase -Filter * -ResultSetSize 1000

#Loop Through OU's Accounts
foreach($a in $Accounts)
{
    #Grab UPN and SamAccountName
    $UPN = $a.UserPrincipalName
    $SamAccountName = $a.SamAccountName

    #Pull existing SMTP Addresses
    $GUID = Get-ADUser $SamAccountName | Select ObjectGUID
    $UAID = Get-ADUser $SamAccountName -Properties * | Select uaid

    $GUID = $GUID.ObjectGUID
    $UAID = $UAID.uaid


    if($GUID -eq $UAID)
    {
        $Exists = 1
        $count = $count + 1
    }


    #Add uaid Property if missing
    if($Exists -eq 0)
    {
        Get-ADUser $SamAccountName | ForEach-Object {Set-ADUser $_.SamAccountName -Add @{uaid=$_.ObjectGUID}}
        Write-Host "Added uaid to: $SamAccountName
    }
}
