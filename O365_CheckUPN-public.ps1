﻿<#
.SYNOPSIS
  This script searches an OU and to make sure all UPN's are set to @email.arizona.edu. This script only writes the incorrect $UPNs without making any changes.
.DESCRIPTION
  This script searches an OU and to make sure all UPN's are set to @email.arizona.edu. This script only writes the incorrect $UPNs without making any changes.
  ResultSetSize is set to 1000 and can be increased if needed.
#>

#Script Name Check_O365UPN.ps1
#Creator Shawn Kolofer

#Declare Variables
$SearchBase = “OU=Delegated OUs,OU=Delegation,DC=catnet,DC=arizona,DC=edu” #setThis
$Accounts = Get-ADUser -SearchBase $SearchBase -Filter * -ResultSetSize 1000
$O365Suffix = "email.arizona.edu"

#Loop Through OU's Accounts
foreach($a in $Accounts)
{
    #Grab UPN
    $UPN = $a.UserPrincipalName

    #Get Suffix of UPN
    $UPNSplit = $UPN -split('@')
    $Suffix = $UPNSplit[1]

    #Write incorrect UPNs
    if($Suffix -ne $O365Suffix)
    {
        Write-Host "Incorrect UPN: $UPN"
    }
}
