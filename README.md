# README #

This README would normally document whatever steps are necessary to get your application up and running.

### List of Scripts ###

* O365_AddAlias-public.ps1
* O365_Adduaid-public.ps1
* O365_CheckUPN-public.ps1
* Calendar_Audit-public.ps1
* SharedBox_Audit-public.ps1

### O365_AddAlias-public.ps1 ###
-This script will search all accounts in an OU and look for the @emailarizona.mail.onmicrosoft.com alias. If the alias does not exist, the script will add it using the SamAccountName and O365 suffix. The code needs to be copy/pasted to the Exchange Management Shell in order for it to execute.

* Variable that needs to be set
	- $SearchBase
		- Set this to the OU that you want to query
		- Example: $SearchBase = “OU=UAConnect,OU=DEPT,OU=Delegated OUs,OU=Delegation,DC=catnet,DC=arizona,DC=edu”



### O365_Adduaid-public.ps1 ###
-This script will search all accounts in an OU and look for the uaid attribute needed for O365's AD Connect. If the uaid value is null/incorrect, the script will populate it with the ObjectGUID. 

* Variable that needs to be set
	- $SearchBase
		- Set this to the OU that you want to query
		- Example: $SearchBase = “OU=UAConnect,OU=DEPT,OU=Delegated OUs,OU=Delegation,DC=catnet,DC=arizona,DC=edu”



### O365_CheckUPN-public.ps1 ###
-This script searches an OU and to make sure all UPN's are set to @email.arizona.edu. This script only writes an ouput of the incorrect $UPNs and will not make any changes. 

* Variable that needs to be set
	- $SearchBase
		- Set this to the OU that you want to query
		- Example: $SearchBase = “OU=UAConnect,OU=DEPT,OU=Delegated OUs,OU=Delegation,DC=catnet,DC=arizona,DC=edu”



### Calendar-Audit-public.ps1 ###
-This Script searches an OU to audits shared calendars. All shared calendars would have to exist in the same OU. It will pull delegated admins, calendar permissions, and send an email to all admins.  This needs to be copy/pasted to the Exchange Management Shell in order for it to execute. After testing is complete, you will want to comment out the send-mailmessage on line 192, and uncomment the send-mailmessage on line 189 in order to send the email to all delegated admins.

* Variables that needs to be set
	- $SearchBase
		- Set this to the OU that you want to query
	- $FromEmailAddress	
		- Set this to the sender's email address
	- $BccEmailAddress
		- Set this to an email address that you want Bcc'd on the emails
	- $ToTestEmail
		- Set this to your email for testing purposes.
	- $EmailSubject
		- Modifies the Subject of the email
	- $Intro
		- Modifies the begginning body text
	- $Closing
		- Modifes the text after the table has been inserted
	- $Signature
		- Modifies the signature at the bottom
	
	- Examples: 
		- $SearchBase = “OU=Calendar,OU=UAConnect,OU=DEPT,OU=Delegated OUs,OU=Delegation,DC=catnet,DC=arizona,DC=edu”
		- $FromEmailAddress = "support@yourdept.com"
		- $ToTestEmail = "myemail@email.arizona.edu"
		- $EmailSubject = "DEPT - Shared Calendar Audit"
		- $Intro = "DEPT is in the process of auditing all shared calendar and mailboxes in preparation for the UA Office 365 email migration. If this mailbox is no longer needed, please reply to this email saying so. Below is a 			list everyone who currently has access to the calendar."
		- $Closing = "Thank you for taking the time to help us keep DEPT calendars and mailboxes current. Please let us know if you have any questions or concerns."
		- $Signature = "--<br>DEPT IT<br>The University of Arizona"



### SharedBox_Audit-public.ps1 ###
-This Script searches an OU to audits shared mailboxes. All shared mailboxes would have to exist in the same OU for this to work correctly. It will pull delegated admins, inbox permissions, and send an emails all admins.  This needs to be copy/pasted to the Exchange Management Shell in order for it to execute. After testing is complete, you will want to comment out the send-mailmessage on line 195, and uncomment the send-mailmessage on line 192 in order to send the email to all delegated admins.

* Variables that needs to be set
	- $SearchBase
		- Set this to the OU that you want to query
	- $FromEmailAddress	
		- Set this to the sender's email address
	- $BccEmailAddress
		- Set this to an email address that you want Bcc'd on the emails
	- $ToTestEmail
		- Set this to your email for testing purposes.
	- $EmailSubject
		- Modifies the Subject of the email
	- $Intro
		- Modifies the begginning body text
	- $Closing
		- Modifes the text after the table has been inserted
	- $Signature
		- Modifies the signature at the bottom
	
	- Examples: 
		- $SearchBase = “OU=Shared,OU=UAConnect,OU=DEPT,OU=Delegated OUs,OU=Delegation,DC=catnet,DC=arizona,DC=edu”
		- $FromEmailAddress = "support@yourdept.com"
		- $ToTestEmail = "myemail@email.arizona.edu"
		- $EmailSubject = "DEPT - Shared Calendar Audit"
		- $Intro = "DEPT is in the process of auditing all shared calendar and mailboxes in preparation for the UA Office 365 email migration. If this mailbox is no longer needed, please reply to this email saying so. Below is a 			list everyone who currently has access to the calendar."
		- $Closing = "Thank you for taking the time to help us keep DEPT calendars and mailboxes current. Please let us know if you have any questions or concerns."
		- $Signature = "--<br>DEPT IT<br>The University of Arizona"

### listSharedObjectPermissions.ps1 ###
-This script will grab all of the nono-default permissions to an Exchange object like a room calendar, or a shared mailbox.  It outputs the permissions in to a semi-colon separated file.  There are mailbox permissions on the inbox, calendar permissions on the default calendar, and delegation permissions assigned via the Exchange Admin Console.

* Variables that need to be set
	- $searchbase
		- This is the OU the script will pull objects from.
	- $outputFileName
		- This is the name of the file the script will output the permissions list to.

### listUserMailboxPermissions.ps1 ###
-This script will grab all of the non-default permissions to an Exchange object such as a user mailbox.  It outputs the permissions in to a semi-colon separated file.  There are mailbox permissions on the inbox, and calendar permissions on the default calendar.

* Variables that need to be set
	- $searchGroup
		- This is the security group the script will pull objects from.  We recommend setting this to something like your scope group in CatNet > Enterprise > Groups > Exchange > NetIDScopeGroups.
	- $outputFileName
		- This is the name of the file the script will output the permissions to list.