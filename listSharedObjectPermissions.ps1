<#
.SYNOPSIS
  Script searches an OU to audits shared Exchange objects, and outputs a list of added permissions to the inbox, default calendar, or delegated permissions.
.DESCRIPTION
  Script searches an OU to audits shared Exchange objects, like calendars and mailboxes. It pulls permissions to the inbox, the default calendar, and those delegated through the Exchange Admin Console. This script must be run from Exchange Management Shell.  Output is saved to a semi-colon separated file.
#>

#Script Name listSharedObjectPermissions.ps1
#Creator Paul Reeves (based on scripts from Shawn Kolofer)

#Declare Variables
$searchBase = “OU=Delegated OUs,OU=Delegation,DC=catnet,DC=arizona,DC=edu” #Set this to the OU you want the script to pull from.
$sharedExchangeObjects = Get-ADUser -SearchBase $searchBase -Filter *
$NetIDRegEx = [regex]"((?<=\()[a-z0-9]{1,20}(?=\)))"
$outputFileName = 'sharedObjectPermissions.txt'

'Shared Object Name; Mailbox/Calendar/Delegation; Access Rights; Permission Holder NetID; Permission Holder Name' | Out-file $outputFileName

#Loop through each object in the OU
foreach ($sharedExchangeObject in $sharedExchangeObjects){

  #Test to make sure whether a mailbox exists for that object.
  $mailboxExists = [bool](get-mailbox -recipienttype usermailbox,sharedmailbox,roommailbox,EquipmentMailbox,GroupMailbox $sharedExchangeObject.name -erroraction SilentlyContinue)
  if (-not $mailboxExists){continue}

  #Get Calendar permissions
  $sharedObject = $sharedExchangeObject.SamAccountName + ":\Calendar"
  $sharedObjectPermissions = Get-MailboxFolderPermission -identity $sharedObject | where {$_.user.tostring() -ne "Default"} | where {$_.user.tostring() -ne "Anonymous"} | Select User,AccessRights

  #Output calendar permissions
  foreach ($sharedObjectPermission in $sharedObjectPermissions){

    $match = $NetIDRegEx.match($sharedObjectPermission.User.DisplayName)
    $permissionHolderNetID = $match.Value

    $permissionHolderFullName = $sharedObjectPermission.User.DisplayName.substring(0, $sharedObjectPermission.User.DisplayName.length - ($permissionHolderNetID.length +4 ))

    if ($permissionHolderNetID.length -eq 0){$permissionHolderFullName = $sharedObjectPermission.User.DisplayName }

    $sharedExchangeObject.name + "; Calendar; " + $sharedObjectPermission.AccessRights + "; " + $permissionHolderNetID + "; " + $permissionHolderFullName | out-file -append $outputFileName
  }

  #Get mailbox permissions
  $sharedObject = $sharedExchangeObject.SamAccountName + ":\Inbox"
  $sharedObjectPermissions = Get-MailboxFolderPermission -identity $sharedObject | where {$_.user.tostring() -ne "Default"} | where {$_.user.tostring() -ne "Anonymous"} | Select User,AccessRights

  #Output calendar permissions
  foreach ($sharedObjectPermission in $sharedObjectPermissions){

    $match = $NetIDRegEx.match($sharedObjectPermission.User.DisplayName)
    $permissionHolderNetID = $match.Value

    $permissionHolderFullName = $sharedObjectPermission.User.DisplayName.substring(0, $sharedObjectPermission.User.DisplayName.length - ($permissionHolderNetID.length +4 ))

    if ($permissionHolderNetID.length -eq 0){$permissionHolderFullName = $sharedObjectPermission.User.DisplayName }

    $SharedExchangeObject.name + "; Mailbox; " + $sharedObjectPermission.AccessRights + "; " + $permissionHolderNetID + "; " + $permissionHolderFullName | out-file -append $outputFileName
  }

  #Get permissions delegated through EAC
  $delegates = Get-Mailbox -identity $sharedExchangeObject.Name | Get-MailboxPermission | where {$_.user.tostring() -ne "NT AUTHORITY\SELF" -and $_.IsInherited -eq $false} | Select User,AccessRights

  #Output permissions delegated through EAC
  foreach ($delegate in $delegates){

    $permissionHolder = get-aduser $delegate.User.Replace("CATNET\","")
    $sharedExchangeObject.name + "; Delegation; " + $delegate.AcessRights + "; " +$permissionHolder.samaccountname + $permissionHolder.name.substring(0, $permissionHolder.name.length - ($permissionHolder.samaccountname.length + 4))| out-file -append $outputFileName
  }
}
